/**
 * Winston Logger Module
 */

const winston = require('winston')
const path = require('path')

// Configure custom app-wide logger
module.exports = new winston.Logger({
  transports: [
    new (winston.transports.Console)({ colorize: true }),
    new (winston.transports.File)({
      name: 'info-file',
      filename: path.resolve(__dirname, '../info.log'),
      level: 'info',
      json: true,
      maxsize: 5242880, //5MB
      maxFiles: 5
    }),
    new (winston.transports.File)({
      name: 'error-file',
      filename: path.resolve(__dirname, '../error.log'),
      level: 'error',
      json: true,
      maxsize: 5242880, //5MB
      maxFiles: 5
    })
  ]
})