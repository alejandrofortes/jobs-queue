/**
 * Job Object
 */
class Job {

    /** Job Constructor
    * @param { Number } id - Job id
    * @param { String } name - Job name
    * @param { Number } algorithm - Algorithm to execute
    */
    constructor (id, name, algorithm) {

        this.id = id
        this.name = name
        this.algorithm = algorithm
        this.date = new Date()
        this.status = 'waiting'
    }
}

module.exports = Job