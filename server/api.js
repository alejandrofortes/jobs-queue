/**
 * API Routes Module
 */
const Router = require('koa-router')
const queue = require('./queue')
const joi = require('joi')
const validate = require('koa-joi-validate')
const router = new Router()

// Hello World Test Endpoint
router.get('/hello', async ctx => {
  
  ctx.body = {
    data: "Hello World"
  }
  
})

// Get all jobs
router.get('/show', async ctx => {
  
  const result = await queue.show()
  ctx.body = result

})

// Enqueue a job
router.post('/enqueue/:id/:name/:algorithm', 
  validate({
    params: {
      id: joi.string().required(),
      name: joi.string().required(),
      algorithm: joi.string().required()
    }
  }), async ctx => {
  
  const id = ctx.params.id
  const name = ctx.params.name
  const algorithm = ctx.params.algorithm
  const result = await queue.enqueue(id, name, algorithm)
  ctx.body = result

})

// Dequeue the top of the queue
router.post('/dequeue', async ctx => {

  const result = await queue.dequeue()
  ctx.body = result

})

// Get the top of the queue (the next job)
router.get('/top', async ctx => {

  if (await queue.isEmpty()) { ctx.throw(404, 'Empty queue') }

  const result = await queue.top()
  ctx.body = result

})

// Updates the job status
router.put('/update/:id/:status', 
  validate({
    params: {
      id: joi.string().required(),
      status: joi.number().min(0).max(1).required()
    }
  }), async ctx => {

  if (await queue.isEmpty()) { ctx.throw(404, 'Empty queue') }

  const id = ctx.params.id
  const status = ctx.params.status

  const top = await queue.top()
  if (id != top.id) { ctx.throw(400, 'The given job is not the top of the queue') }

  var result = null
  if (status == 0) {
    result = await queue.update(status)
  } else if (status == 1) {
    result = await queue.dequeue()
  }
  ctx.body = result

})

// Removes a job by id
router.delete('/remove/:id', 
  validate({ 
    params: { id: joi.string().required() }
  }), async ctx => {

  if (await queue.isEmpty()) { ctx.throw(404, 'Empty queue') }

  const id = ctx.params.id
  
  const result = await queue.remove(id)
  
  if (!result) { ctx.throw(404, 'Job not found') }

  ctx.body = result

})

// Removes all jobs from the queue
router.delete('/empty', async ctx => {

  const result = await queue.empty()
  ctx.body = result

})

// Get all errors
router.get('/errors', async ctx => {
  
  const result = await queue.errors()
  ctx.body = result

})

// Add a job to the errors list
router.post('/error/:id/:description',
  validate({
    params: {
      id: joi.string().required(),
      description: joi.string().required()
    }
  }), async ctx => {
    
  if (await queue.isEmpty()) { ctx.throw(404, 'Empty queue') }

  const id = ctx.params.id
  const description = ctx.params.description
  
  const result = await queue.addError(id, description)
  
  if (!result) { ctx.throw(404, 'Job not found') }

  ctx.body = result

})

// Moves a job from the errors list to the queue
router.post('/recover/:id',
  validate({
    params: { id: joi.string().required() }
  }), async ctx => {

  const id = ctx.params.id

  const result = await queue.recover(id)

  if (!result) { ctx.throw(404, 'Job not found') }

  ctx.body = result

})

// Removes a job from the errors list
router.delete('/discard/:id', 
  validate({
    params: { id: joi.string().required() }
  }), async ctx => {

  const id = ctx.params.id

  const result = await queue.discard(id)

  if (!result) { ctx.throw(404, 'Job not found') }

  ctx.body = result
  
})

// Removes all jobs from the errors list
router.delete('/emptyErrors', async ctx => {

  const result = await queue.emptyErrors()
  ctx.body = result

})

module.exports = router