const Job = require('./job')

/**
 * Error Object
 */
class Error {

    /**
     * Error constructor
     * @param { Number } id - Job id
     * @param { Job } Job - Job with an error 
     * @param { String } description - Error description
     */
    constructor (id, Job, description) {

        this.id = id
        this.Job = Job
        this.description = description
    }
}

module.exports = Error