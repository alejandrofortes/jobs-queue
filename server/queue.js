/**
 * Queue Module
 */
const log = require('./logger')
const Job = require('./job')
const Error = require('./error')

// Initialize queue
var queue = []
log.info(`Queue created`)

// Initialize errors list
var errors = []
log.info(`Errors list created`)

getIndex = (id, list) => {

  var index = 0
  var found = false

  list.forEach(function(job) {
      
    if (job.id != id && !found) {      
      index++
    } else {
      found = true
    }
  })

  return found ? index : -1
}

module.exports = {

  /** Get all jobs */
  show: async () => {
    
    return queue
  },

  /** Enqueue a job */
  enqueue: async (id, name, algorithm) => {

    const element = new Job(id, name, algorithm)
    queue.push(element)
  },

  /** Dequeue the top of the queue */
  dequeue: async () => {

    queue.shift()
  },

  /** Get the top of the queue (the next job) */
  top: async () => {

    return queue[0]
  },

  /** Check if the queue is empty */
  isEmpty: async () => {

    return !queue.length
  },
  
  /** Updates the job status */
  update: async (status) => {

    if (status == 0) { queue[0].status = 'in progress' }
    
  },

  /** Removes a job from the queue */
  remove: async (id) => {

    const index = getIndex(id, queue)

    if (index ==  -1) { return false }

    queue.splice(index, 1)

    return true
  },

  /** Removes all jobs from the queue */
  empty: async () => {

    queue = []
  },

  /** Get all errors */
  errors: async () => {
    
    return errors
  },

  /** Add a job to the errors list */
  addError: async (id, description) => {

    const index = getIndex(id, queue)

    if (index ==  -1) { return false }

    // Update job status
    var element = queue[index]
    element.status = 'error'

    const error = new Error(element.id, element, description)

    errors.push(error)
    queue.splice(index, 1)

    return true
  },

  /** Moves a job from the errors list to the queue */
  recover: async (id) => {

    const index = getIndex(id, errors)

    if (index ==  -1) { return false }

    const element = errors[index]

    // Update job status
    var job = element.Job
    job.status = 'waiting'

    queue.push(job)
    errors.splice(index, 1)

    return true
  },

  /** Removes a job from the errors list */
  discard: async (id) => {

    const index = getIndex(id, errors)

    if (index ==  -1) { return false }

    errors.splice(index, 1)

    return true
  },

  /** Removes all jobs from the errors list */
  emptyErrors: async () => {

    errors = []
  }
}