/**
 * Test API Routes Module
 */

const server = require("../server/index");
const request = require("supertest");

// close the server after each test
afterEach(() => {
  server.close()
})

describe("routes", () => {
  
  // Test Hello World endpoint.
  test("Hello World endpoint test", async () => {
    
    const response = await request(server).get("/hello")
    expect(response.status).toEqual(200)
    expect(response.type).toEqual("application/json")
    expect(response.body.data).toEqual("Hello World")
  })

  // The initial state of the queue is empty, so when geting
  // the top of the queue, an error will be obtained.
  test("Top test", async () => {
    
    await request(server).delete("/empty")

    const response = await request(server).get("/top")
    expect(response.status).toEqual(404)
    expect(response.type).toEqual("text/plain")
    expect(response.error.text).toEqual("Empty queue")
  })

  // The initial state of the queue is empty.
  // Show queue will give an empty list.
  test("Show test", async () => {
    
    await request(server).delete("/empty")

    const response = await request(server).get("/show")
    expect(response.status).toEqual(200)
    expect(response.type).toEqual("application/json")
    expect(response.body).toEqual([])
  })
  
  // Enqueue a job should response an 204 code, the top of the
  // queue should response a json with several fields, and the
  // queue must have size one.
  test("Enqueue test", async () => {
    
    await request(server).delete("/empty")

    var response = await request(server).post("/enqueue/1/test/1")
    expect(response.status).toEqual(204)
    
    response = await request(server).get("/top")
    expect(response.status).toEqual(200)
    expect(response.type).toEqual("application/json")
    expect(Object.keys(response.body)).toEqual(
      expect.arrayContaining(["id", "name", "algorithm", "date", "status"])
    )

    response = await request(server).get("/show")
    expect(response.status).toEqual(200)
    expect(response.type).toEqual("application/json")
    expect(response.body).toHaveLength(1)
  })
  
  // Dequeue should remove the top of the queue.
  test("Dequeue test", async () => {

    await request(server).delete("/empty")

    await request(server).post("/enqueue/1/test1/1")
    await request(server).post("/enqueue/2/test2/1")

    var top = await request(server).get("/top")
    expect(top.body.id).toEqual("1")
    expect(top.body.name).toEqual("test1")

    var dequeue = await request(server).post("/dequeue")
    expect(dequeue.status).toEqual(204)

    top = await request(server).get("/top")
    expect(top.body.id).toEqual("2")
    expect(top.body.name).toEqual("test2")
  })

  // Update the queue when it is empty, returns an 404 code.
  // Updating with an id which is not the top of the queue 
  // return an error.
  // When the second parameter of the update url is 0,
  // then the status of the top is "in progress".
  // When the second parameter of the update url is 1,
  // the top of the queue is removed.
  test("Update test", async () => {
    
    await request(server).delete("/empty")

    var response = await request(server).put("/update/mock/0")
    expect(response.status).toEqual(404)
    expect(response.error.text).toEqual("Empty queue")

    await request(server).post("/enqueue/1/test1/1")
    
    var error = await request(server).put("/update/mock/0")
    expect(error.status).toEqual(400)
    expect(error.error.text).toEqual("The given job is not the top of the queue")

    var top = await request(server).get("/top")
    expect(top.body.status).toEqual("waiting")

    await request(server).put("/update/" + top.body.id + "/0")
    
    top = await request(server).get("/top")
    expect(top.body.status).toEqual("in progress")

    await request(server).put("/update/" + top.body.id + "/1")
    var show = await request(server).get("/show")
    expect(show.body).toEqual([])
  })

  // Remove when the queue is empty, returns an 404 code.
  // Remove a non existent job id, returns an error.
  // Remove a job, make it disappear from the queue.
  test("Remove test", async () => {
  
    await request(server).delete("/empty")

    var response = await request(server).delete("/remove/mock")
    expect(response.status).toEqual(404)
    expect(response.error.text).toEqual("Empty queue")

    await request(server).post("/enqueue/1/test1/1")
    await request(server).post("/enqueue/2/test2/1")

    response = await request(server).delete("/remove/mock")
    expect(response.status).toEqual(404)
    expect(response.error.text).toEqual("Job not found")

    var queue = await request(server).get("/show")
    var id = queue.body[1].id

    var remove = await request(server).delete("/remove/" + id)
    expect(remove.status).toEqual(200)

    var queue2 = await request(server).get("/show")  
    expect(queue2.body).toHaveLength(1)
    expect(queue2.body[0].id).toEqual(queue.body[0].id)
  })

  // Emptying the queue makes its size zero.
  test("Empty test", async () => {

    await request(server).delete("/empty")
    
    await request(server).post("/enqueue/1/test1/1")
    await request(server).post("/enqueue/2/test2/1")

    var response = await request(server).get("/show")
    expect(response.body).toHaveLength(2)

    await request(server).delete("/empty")

    response = await request(server).get("/show")  
    expect(response.body).toHaveLength(0)
  })

  // The initial state of the errors list is empty.
  // Show errors list will give an empty list.
  test("Empty errors test", async () => {

    await request(server).delete("/emptyErrors")

    const response = await request(server).get("/errors")
    expect(response.status).toEqual(200)
    expect(response.type).toEqual("application/json")
    expect(response.body).toEqual([])
  })

  // Add an error when the queue is empty, returns a 404 code.
  // Add a job to the errors list from an non existent job id, 
  // returns an error.
  // When a job is added to the errors list, the job is removed
  // from the queue.
  // The status of the jobs of the errors list is 'error'. 
  test("Add error test", async () => {

    await request(server).delete("/empty")
    await request(server).delete("/emptyErrors")

    var response = await request(server).post("/error/i/d")
    expect(response.status).toEqual(404)

    await request(server).post("/enqueue/1/test1/1")
    var queue = await request(server).get("/show")
    var id = queue.body[0].id

    response = await request(server).post("/error/mockid/d")
    expect(response.status).toEqual(404)
    expect(response.error.text).toEqual("Job not found")

    response = await request(server).post("/error/" + id + "/d")
    queue = await request(server).get("/show")
    expect(queue.status).toEqual(200)
    expect(queue.body).toEqual([])

    var errors = await request(server).get("/errors")
    expect(errors.status).toEqual(200)
    expect(errors.body).toHaveLength(1)
    expect(errors.body[0].id).toEqual(id)
    expect(errors.body[0].description).toEqual("d")
    expect(errors.body[0].Job.name).toEqual("test1")
    expect(errors.body[0].Job.status).toEqual("error")
  })

  // Recover an error when the errors list is empty returns an error.
  // Recover an error from an non existent job id, returns a 404 code.
  // When an job is recovered from the errors list, the error is 
  // removed from the errors list and is added to the queue.
  // The status of the recovered job is 'waiting'.
  test("Recover test", async () => {

    await request(server).delete("/empty")
    await request(server).delete("/emptyErrors")

    var response = await request(server).post("/recover/id")
    expect(response.status).toEqual(404)

    await request(server).post("/enqueue/1/test1/1")
    var queue = await request(server).get("/show")
    var id = queue.body[0].id

    response = await request(server).post("/recover/id")
    expect(response.status).toEqual(404)
    expect(response.error.text).toEqual("Job not found")

    await request(server).post("/error/" + id + "/desc")

    response = await request(server).post("/recover/" + id)
    expect(response.status).toEqual(200)

    queue = await request(server).get("/show")
    expect(queue.body).toHaveLength(1)
    expect(queue.body[0].id).toEqual(id)
    expect(queue.body[0].status).toEqual("waiting")
    
    var errors = await request(server).get("/errors")
    expect(errors.status).toEqual(200)
    expect(errors.body).toHaveLength(0)
  })

  // Discard an error when the errors list is empty returns an error.
  // Discard an error from an non existent job id, returns a 404 code.
  // When a discard is done, the job is removed from the errors list.
  test("Discard test", async () => {

    await request(server).delete("/empty")
    await request(server).delete("/emptyErrors")

    var response = await request(server).post("/discard/id")
    expect(response.status).toEqual(404)

    await request(server).post("/enqueue/1/test1/1")
    var queue = await request(server).get("/show")
    var id = queue.body[0].id

    response = await request(server).delete("/discard/id")
    expect(response.status).toEqual(404)
    expect(response.error.text).toEqual("Job not found")

    await request(server).post("/error/" + id + "/desc")

    response = await request(server).delete("/discard/" + id)
    expect(response.status).toEqual(200)

    var errors = await request(server).get("/errors")
    expect(errors.status).toEqual(200)
    expect(errors.body).toHaveLength(0)
  })

  // Emptying the errors list makes its size zero.
  test("Empty errors test", async () => {

    await request(server).delete("/empty")
    await request(server).delete("/emptyErrors")

    await request(server).post("/enqueue/1/test1/1")
    await request(server).post("/enqueue/2/test2/2")

    var queue = await request(server).get("/show")
    var id1 = queue.body[0].id
    var id2 = queue.body[1].id

    await request(server).post("/error/" + id1 + "/desc1")
    await request(server).post("/error/" + id2 + "/desc2")

    var errors = await request(server).get("/errors")
    expect(errors.body).toHaveLength(2)

    var response = await request(server).delete("/emptyErrors")
    expect(response.status).toEqual(204)

    errors = await request(server).get("/errors")
    expect(errors.status).toEqual(200)
    expect(errors.type).toEqual("application/json")
    expect(errors.body).toEqual([])
  })
})