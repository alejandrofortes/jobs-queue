# Jobs queue

A simple jobs queue implementation with Node.js

#### Structure
- `server/` - The Node.js API server code.

#### Setup

To setup the project, simply download or clone the project to your local machine and `npm install`.

The only extra step is adding a `.env` file in order to properly initialize the required environment variables.

Here's an example `.env` file with sensible defaults for local development -
```
PORT=5001
CORS_ORIGIN=http://localhost:9090
```


Run `npm start` to start the API server on `localhost:5001`, and to build/watch/serve the frontend code from `localhost:9090`.